package com.geekhub.season4.kliuch.service;

import com.geekhub.season4.kliuch.Page;
import com.geekhub.season4.kliuch.PageRequest;
import com.geekhub.season4.kliuch.Range;
import com.geekhub.season4.kliuch.entity.User;
import com.geekhub.season4.kliuch.repository.UserRepository;

import java.util.List;

public class CustomUserService implements UserService {
    private UserRepository userRepository;

    public CustomUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Page<User> getUsers(PageRequest pageRequest) {
        Range range = createRange(pageRequest.getPageIndex(), pageRequest.getResultsPerPage());
        List<User> users = userRepository.findUsersInRage(range);
        int currentPageIndex = pageRequest.getPageIndex();
        int totalResults = userRepository.count();
        return new Page<>(currentPageIndex, totalResults, users);
    }

    private Range createRange(int pageIndex, int resultsPerPage) {
        int from = (pageIndex - 1) * resultsPerPage;
        int to = from + resultsPerPage;
        return new Range(from, to);
    }
}
