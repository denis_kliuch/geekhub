package com.geekhub.season4.kliuch;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class PaginationTagHandler extends SimpleTagSupport {
    private int resultsPerPage;
    private int totalResults;
    private int currentPageIndex;
    private String pageUrl;

    private StringBuilder content = new StringBuilder();

    @Override
    public void doTag() throws JspException, IOException {
        getJspContext().getOut().println(generateContent());
    }

    private String generateContent() {
        int pageCount = getPageCount();
        for (int i = 1; i < pageCount; i++) {
            if (i == currentPageIndex) {
                highlightCurrentPage();
            } else {
                addLink(i);
            }
        }
        return content.toString();
    }

    private void highlightCurrentPage() {
        content.append("<b>").append(currentPageIndex).append("</b>");
    }

    private void addLink(int pageIndex) {
        content
                .append("<a href='")
                .append(getPageUrlFor(pageIndex))
                .append("'>")
                .append(pageIndex)
                .append("</a>");
    }

    private String getPageUrlFor(int pageIndex) {
        return pageUrl.replace("{page}", pageIndex + "");
    }

    private int getPageCount() {
        int pageCount;
        if (totalResults % resultsPerPage == 0) {
            pageCount = totalResults / resultsPerPage;
        } else {
            pageCount = totalResults / resultsPerPage + 1;
        }
        return pageCount;
    }

    public void setResultsPerPage(int resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public void setCurrentPageIndex(int currentPageIndex) {
        this.currentPageIndex = currentPageIndex;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }
}
