package com.geekhub.season4.kliuch.repository;

import com.geekhub.season4.kliuch.Range;
import com.geekhub.season4.kliuch.entity.User;

import java.util.ArrayList;
import java.util.List;

public class InMemoryUserRepository implements UserRepository {
    private static List<User> users = new ArrayList<>();

    static {
        for (int i = 0; i < 100; i++) {
            users.add(new User(i + 1, "User" + (i + 1)));
        }
    }

    @Override
    public List<User> findUsersInRage(Range range) {
        return users.subList(range.getFrom(), range.getTo());
    }

    @Override
    public int count() {
        return users.size();
    }
}
