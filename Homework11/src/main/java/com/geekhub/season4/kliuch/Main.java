package com.geekhub.season4.kliuch;

import com.geekhub.season4.kliuch.entity.User;
import com.geekhub.season4.kliuch.repository.InMemoryUserRepository;
import com.geekhub.season4.kliuch.repository.UserRepository;
import com.geekhub.season4.kliuch.service.CustomUserService;
import com.geekhub.season4.kliuch.service.UserService;

public class Main {
    private static final int RESULT_PER_PAGE = 5;

    public static void main(String[] args) {
        UserRepository userRepository = new InMemoryUserRepository();
        UserService userService = new CustomUserService(userRepository);
        Page<User> page = userService.getUsers(new PageRequest(4, RESULT_PER_PAGE));
        System.out.println(page.getContent());
//        System.out.println(page.);
    }
}
