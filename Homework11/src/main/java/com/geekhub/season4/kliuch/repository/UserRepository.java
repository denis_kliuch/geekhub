package com.geekhub.season4.kliuch.repository;

import com.geekhub.season4.kliuch.Range;
import com.geekhub.season4.kliuch.entity.User;

import java.util.List;

public interface UserRepository {
    List<User> findUsersInRage(Range range);
    int count();
}
