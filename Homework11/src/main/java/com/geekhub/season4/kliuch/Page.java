package com.geekhub.season4.kliuch;

import com.geekhub.season4.kliuch.entity.User;

import java.util.List;

public class Page<T> {
    private int currentPageIndex;
    private int totalResults;
    private List<User> content;

    public Page(int currentPageIndex, int totalResults, List<User> content) {
        this.currentPageIndex = currentPageIndex;
        this.totalResults = totalResults;
        this.content = content;
    }

    public int getCurrentPageIndex() {
        return currentPageIndex;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public List<User> getContent() {
        return content;
    }
}
