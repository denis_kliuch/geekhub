package com.geekhub.season4.kliuch.web;

import com.geekhub.season4.kliuch.Page;
import com.geekhub.season4.kliuch.PageRequest;
import com.geekhub.season4.kliuch.entity.User;
import com.geekhub.season4.kliuch.repository.InMemoryUserRepository;
import com.geekhub.season4.kliuch.service.CustomUserService;
import com.geekhub.season4.kliuch.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/users")
public class UserServlet extends HttpServlet {
    private static final String PAGE_INDEX_PARAMETER = "page";
    private static final int RESULT_PER_PAGE = 10;

    private UserService userService;

    public UserServlet() {
        userService = new CustomUserService(new InMemoryUserRepository());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int pageIndex = getPageIndex(request);
        Page<User> page = userService.getUsers(new PageRequest(pageIndex, RESULT_PER_PAGE));
        addContent(request, page);
        request.getRequestDispatcher("/WEB-INF/jsp/users.jsp").forward(request, response);
    }

    private int getPageIndex(HttpServletRequest request) {
        int pageIndex;
        try {
            pageIndex = Integer.parseInt(request.getParameter(PAGE_INDEX_PARAMETER));
        } catch (NumberFormatException e) {
            pageIndex = 1;
        }
        return pageIndex;
    }

    private void addContent(HttpServletRequest request, Page<User> page) {
        request.setAttribute("resultsPerPage", RESULT_PER_PAGE);
        request.setAttribute("totalResults", page.getTotalResults());
        request.setAttribute("currentPageIndex", page.getCurrentPageIndex());
        request.setAttribute("pageUrl", "users?page={page}");
        request.setAttribute("users", page.getContent());
    }
}