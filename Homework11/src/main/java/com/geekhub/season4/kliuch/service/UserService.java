package com.geekhub.season4.kliuch.service;

import com.geekhub.season4.kliuch.Page;
import com.geekhub.season4.kliuch.PageRequest;
import com.geekhub.season4.kliuch.entity.User;

public interface UserService {
    Page<User> getUsers(PageRequest pageRequest);
}