package com.geekhub.season4.kliuch;

public class PageRequest {
    private int pageIndex;
    private int resultsPerPage;

    public PageRequest(int pageIndex, int resultsPerPage) {
        this.pageIndex = pageIndex;
        this.resultsPerPage = resultsPerPage;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public int getResultsPerPage() {
        return resultsPerPage;
    }
}
