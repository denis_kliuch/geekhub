<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib uri="/WEB-INF/tld/pagination.tld" prefix="gh"%>
<html>
<head>
    <title>Users</title>
</head>
<body>
<table border="1" cellpadding="5" cellspacing="5">

    <tr>
        <th>Id</th>
        <th>Name</th>
    </tr>
    <c:forEach var="user" items="${users}">
        <tr>
            <td>${user.id}</td>
            <td>${user.username}</td>
        </tr>
    </c:forEach>
</table>

<gh:paginate
        resultsPerPage="${resultsPerPage}"
        totalResults="${totalResults}"
        currentPageIndex="${currentPageIndex}"
        pageUrl="${pageUrl}"/>

</body>
</html>
