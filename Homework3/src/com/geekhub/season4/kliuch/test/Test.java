package com.geekhub.season4.kliuch.test;

public class Test {
    public static void stringTime() {
        long timeStart = 0;
        long timeEnd = 0;
        String string = new String();
        timeStart = System.currentTimeMillis();
        for (int i = 0; i < 99999; i++) {
            string += string;
        }
        timeEnd = System.currentTimeMillis();
        System.out.println("String time difference: " + (timeEnd - timeStart));
    }

    public static void stringBufferTime() {
        long timeStart = 0;
        long timeEnd = 0;
        StringBuffer stringBufferTime = new StringBuffer();
        timeStart = System.currentTimeMillis();
        for (int i = 0; i < 99999; i++) {
            stringBufferTime.append(stringBufferTime);
        }
        timeEnd = System.currentTimeMillis();
        System.out.println("String Buffer time difference: " + (timeEnd - timeStart));
    }

    public static void stringBuilderTime() {
        long timeStart = 0;
        long timeEnd = 0;
        StringBuilder stringBuilder0 = new StringBuilder();
        timeStart = System.currentTimeMillis();
        for (int i = 0; i < 99999; i++) {
            stringBuilder0.append(stringBuilder0);
        }
        timeEnd = System.currentTimeMillis();
        System.out.println("String Builder time difference: " + (timeEnd - timeStart));
    }
}
