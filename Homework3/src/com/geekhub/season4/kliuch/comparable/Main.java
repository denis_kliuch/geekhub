package com.geekhub.season4.kliuch.comparable;

import java.util.Arrays;

public class Main {

    private static Comparable[] sort(Comparable[] arr) {
        Comparable[] sortArr = Arrays.copyOf(arr, arr.length);
        for (int i = 0; i < sortArr.length; i++) {
            for (int j = sortArr.length - 1; j > i; j--) {
                if (sortArr[j].compareTo(sortArr[j - 1]) == -1) {
                    Comparable temp = sortArr[j];
                    sortArr[j] = sortArr[j - 1];
                    sortArr[j - 1] = temp;
                }
            }
        }
        return sortArr;
    }

    public static void main(String[] args) {
        Dog[] dogs = {
                new Dog(20, "Rydik", 250),
                new Dog(25, "Popik", 230),
                new Dog(23, "Jopik", 275),
                new Dog(14, "Fyfik", 205)
        };
        Computer[] computers = {
                new Computer(1259, "aaaa", "bbbb"),
                new Computer(2955, "zzzzz", "iiiii"),
                new Computer(7512, "rrrrr", "qqqqqq"),
                new Computer(1114, "qqqqwe", "sgaga")
        };

        Comparable[] sortDogs = sort(dogs);
        Comparable[] sortComputers = sort(computers);

        for (Comparable dog : sortDogs) {
            System.out.println(dog);
        }

        for (Comparable computer : sortComputers) {
            System.out.println(computer);
        }
    }
}
