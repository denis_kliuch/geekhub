package com.geekhub.season4.kliuch.comparable;

public class Computer implements Comparable {

    private Integer id;
    private String name;
    private String processor;

    public Computer(Integer id, String name, String processor) {
        this.id = id;
        this.name = name;
        this.processor = processor;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getProcessor() {
        return processor;
    }

    @Override
    public int compareTo(Object o) {
        Computer comp = (Computer) o;
        return this.id.compareTo(comp.getId());
    }

    @Override
    public String toString() {
        return "Computer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", processor='" + processor + '\'' +
                '}';
    }
}
