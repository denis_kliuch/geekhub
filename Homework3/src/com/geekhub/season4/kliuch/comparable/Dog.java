package com.geekhub.season4.kliuch.comparable;

public class Dog implements Comparable {

    private Integer age;
    private String name;
    private int weigth;

    public Dog(Integer age, String name, int weigth) {
        this.age = age;
        this.name = name;
        this.weigth = weigth;
    }

    public Integer getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public int getWeigth() {
        return weigth;
    }

    @Override
    public int compareTo(Object o) {
        Dog dog = (Dog) o;
        return this.age.compareTo(dog.getAge());
    }

    @Override
    public String toString() {
        return "Dog{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", weigth=" + weigth +
                '}';
    }
}
