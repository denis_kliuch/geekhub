package com.geekhub.season4.kliuch;

import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class ResourceLoader {

    public List<String> load(File file) {
        List<String> text = new ArrayList<>();
        String line = "";
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            while ((line = bufferedReader.readLine()) != null) {
               text.add(line);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e1) {
            e1.printStackTrace();

        }
        return text;
    }
}