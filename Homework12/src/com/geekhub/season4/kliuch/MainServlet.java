package com.geekhub.season4.kliuch;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        Translator translator = context.getBean(Translator.class);
        String text = req.getParameter("text");
        String translateText = "";
        if (text != null) {
            translateText = translator.translate(text).toString();
        }
        req.setAttribute("text", translateText);
        req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
    }
}





