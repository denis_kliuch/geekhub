package com.geekhub.season4.kliuch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.StringTokenizer;

@Component
public class Translator {

    @Autowired
    private LanguageDetector languageDetector;
    @Autowired
    private Dictionary dictionary;

    private static final String PATH_TO_DICTIONARY_DIR = "///Users/denyskliuch/Desktop/App/src/resources/dictionary/";//todo refactor

    public StringBuilder translate(String text) {
        StringBuilder translateText = new StringBuilder();
        StringTokenizer words = new StringTokenizer(text);
        Map<Language, Map<String, String>> dictionaries = dictionary.getDictionaries(PATH_TO_DICTIONARY_DIR);

        while (words.hasMoreElements()) {
            String word = (String)words.nextElement();
            Language language = languageDetector.checkLanguage(word);
            Map<String, String> dict = null;
            if (language != null) {
                dict = dictionaries.get(language);
                if (dict != null) {
                    String translateWord = dict.get(word);
                    word = translateWord == null ? "" : translateWord;
                    translateText.append(word).append(" ");
                }
            }
        }
        return translateText;
    }
}
