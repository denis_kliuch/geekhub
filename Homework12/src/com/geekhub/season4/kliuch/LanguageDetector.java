package com.geekhub.season4.kliuch;

import org.springframework.stereotype.Component;

@Component
public class LanguageDetector {

    public Language checkLanguage(String word) {
        //magic
        if (word != null && word.length() > 0) {
            char[] chars = word.toCharArray();
            Integer getCharValue = (int) chars[0]; // the same for other chars

            if (getCharValue.intValue() >= 65 && getCharValue.intValue() <= 122) { // English
                return Language.ENGLISH;
            } else if (getCharValue.intValue() >= 1040 && getCharValue.intValue() <= 1103) { // Russian
                return Language.RUSSIAN;
            }
        }
        return null; // TODO throw exception
    }
}
