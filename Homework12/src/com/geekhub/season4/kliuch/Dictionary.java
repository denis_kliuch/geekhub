package com.geekhub.season4.kliuch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class Dictionary {
    @Autowired
    private ResourceLoader resourceLoader;

    public Map<Language, Map<String, String>> getDictionaries(String pathToDir) {
        Map<Language, Map<String, String>> dictionaries = new HashMap<>();
        File[] files = new File(pathToDir).listFiles();
        for (File dict : files) {
            String dictName = dict.getName();
            Language language = Language.valueOf(dictName.toUpperCase());
            dictionaries.put(language, getDictionary(dict.getAbsolutePath()));
        }
        return dictionaries;
    }

    private Map<String, String> getDictionary(String pathToFile) {
        Map<String, String> dictionary = new HashMap<>();
        List<String> textFromDictionary = resourceLoader.load(new File(pathToFile));

        for (String line : textFromDictionary) {
            String[] words = line.split("=");
            dictionary.put(words[0], words[1]);
        }

        return dictionary;
    }
}
