package com.geekhub.season4.kliuch;

import java.util.Scanner;

public class Main {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            menu();
        }
    }

    static void fibonacci() {
        int firstNumber = 0;
        int secondNumber = 1;
        int sum = 0;

        System.out.println("Fibonacci\nEnter the number : ");

        int endNumber = scanner.nextInt();

        for (int i = 0; i < endNumber; i++) {
            sum = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = sum;
            System.out.println((i+1) + ". " + sum);
        }
        System.out.println("Result = " + sum + "\n");
    }

    static void factorial() {
        System.out.println("Factorial\nEnter the number : ");
        int endNumber = scanner.nextInt();
        int result = 0;
        for (int i = 1; i <= endNumber; i++) {
            result += i;
        }
        System.out.println("Result = " + result + "\n");
    }

    static void menu() {
        System.out.println("1. Fibonacci\n2. Factorial \nYou choose: ");

        switch (scanner.nextInt()) {
            case 1:
                fibonacci();
                break;
            case 2:
                factorial();
                break;
        }
    }

}