package com.geekhub.season4.kliuch;

import java.util.HashMap;
import java.util.Scanner;

public class Numbers {

    private static Scanner scanner = new Scanner(System.in);

    private static final String simpleNumbers[] = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
            "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
    private static final String decimalNumber[] = {"twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
    private static final int MAX_SIMPLE_NUMBER = 20;

    public static void main(String[] args) {
        while (true) {
            menu();
        }
    }


    public static void usingArray(int number) {
        if (number < MAX_SIMPLE_NUMBER) {
            System.out.println(simpleNumbers[number]);
        } else {
            String integerPart = decimalNumber[number / 10 - 2];
            System.out.println(integerPart + " " + simpleNumbers[number % 10]);
        }
    }

    public static void usingSwitch(int number) {
        switch (number) {
            case 0:
                System.out.println("zero");
                break;
            case 1:
                System.out.println("one");
                break;
            case 2:
                System.out.println("two");
                break;
            case 3:
                System.out.println("three");
                break;
            case 4:
                System.out.println("four");
                break;
            case 5:
                System.out.println("five");
                break;
            case 6:
                System.out.println("six");
                break;
            case 7:
                System.out.println("seven");
                break;
            case 8:
                System.out.println("eight");
                break;
            case 9:
                System.out.println("nine");
                break;
            default:
                System.out.println("I don't know this number");
        }
    }

    public static void usingHashMap(int number) {
        HashMap<Integer, String> map = new HashMap<Integer, String>();
        map.put(0, "zero");
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        map.put(5, "five");
        map.put(6, "six");
        map.put(7, "seven");
        map.put(8, "eight");
        map.put(9, "nine");

        if (number >= 0 && number <= 9) {
            System.out.println(map.get(number));
        }
    }

    static void menu() {
        System.out.println("1. Arrays statusWeather\n2. Switch statusWeather\n3. Hash map statusWeather\n You choose: ");

        switch (scanner.nextInt()) {
            case 1:
                System.out.println("Input number");
                usingArray(scanner.nextInt());
                break;
            case 2:
                System.out.println("Input number");
                usingSwitch(scanner.nextInt());
                break;
            case 3:
                System.out.println("Input number");
                usingHashMap(scanner.nextInt());
                break;
        }
    }
}