package com.geekhub.season4.kliuch;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class TestServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        StringBuilder content;
        String route = req.getRequestURI();
        String readParam = req.getParameter("read");
        String deleteParam = req.getParameter("delete");

        boolean isRead = readParam != null && readParam.equals("true");
        boolean isDelete = deleteParam != null && deleteParam.equals("true");
        if (isRead) {
            content = getFileText(route);
        } else if (isDelete) {
            deleteFile(route);
            route = route.replace("new2.1?delete=true", "/Users");
            content = getData("/"); // TODO: set correct url
        } else {
            content = getData(route);
        }
        resp.getWriter().print(content);
    }

    private StringBuilder getData(String route) throws IOException {
        StringBuilder data = new StringBuilder();
        File root = new File(route);
        File[] listFiles = root.listFiles();
        if (listFiles != null) {
            addContent(listFiles, data, route);
        }
        return data;
    }

    private void addContent(File[] files, StringBuilder sb, String route) {
        sb.append("<H2><FONT COLOR=TEAL> Total number of files in the choosen directory - " +
                files.length + "</FONT></H2>");
        sb.append("<H3><FONT COLOR=PURPLE>" +
                "Directory path - " + route + "</FONT></H3><HR>");
        sb.append("<TABLE BORDER=0 CELLSPACING=5>");

        addFiles(files, sb, route);

        sb.append("</TABLE><HR>");
    }

    private void addFiles(File[] files, StringBuilder sb, String route) {
        for (File file : files) {
            String type = file.isDirectory()
                    ? " (Directory)" : " (File)";
            sb.append("<TR><TD>")
                    .append(type)
                    .append("</TD><TD>")
                    .append("<a href=\"")
                    .append(route)
                    .append(file.getName())
                    .append(file.isDirectory()
                            ? "/\">" : "?read=true \">")
                    .append(file.getName())
                    .append("<a href=\"")
                    .append(route)
                    .append(file.getName())
                    .append("?delete=true\"> delete</a>")
                    .append("</TD></TR>");
        }
    }

    private StringBuilder getFileText(String route) throws IOException {
        File file = new File(route);
        StringBuilder sb = new StringBuilder();
        try (BufferedReader is = new BufferedReader(new FileReader(file))) {
            String currentLine;
            while ((currentLine = is.readLine()) != null) {
                sb.append(currentLine).append("\n");
            }
        }
        return sb;
    }

    private void deleteFile(String route) {
        File file = new File(route);
        if (file.exists()) {
            file.delete();
        }
    }
}
