package com.geekhub.season4.kliuch;


import java.lang.reflect.Field;

public class CloneCreator<T> {
    public static <T> T creator(T target) throws IllegalAccessException, InstantiationException {
     Class<T> clazz = (Class<T>) target.getClass();
        T instance = clazz.newInstance();
        Field [] fields = clazz.getDeclaredFields();
        for(Field field: fields){
            field.setAccessible(true);
            field.set(instance, field.get(target));
        }
        return instance;
    }
}