package com.geekhub.season4.kliuch;

import java.lang.reflect.Field;

public class BeanComparator<T> {
    public boolean compare(T first, T second) throws IllegalAccessException {
        Field[] fields = first.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (!field.get(first).equals(field.get(second))) {
                return false;
            }
        }
        return true;
    }
}