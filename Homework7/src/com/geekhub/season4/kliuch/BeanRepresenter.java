package com.geekhub.season4.kliuch;

import java.lang.reflect.Field;

public class BeanRepresenter {
    public void represent(Object bean) throws IllegalAccessException {
        System.out.println("Class: " + bean.getClass().getSimpleName());
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            System.out.println("field: " + field.getName() + ", value: " + field.get(bean));
        }
    }
}
