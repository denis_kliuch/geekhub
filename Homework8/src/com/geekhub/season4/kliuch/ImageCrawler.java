package com.geekhub.season4.kliuch;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ImageCrawler downloads all images user specified folder from specified resource.
 * It uses multi threading user make process faster. To start download images you should call downloadImages(String urlToPage) method with URL.
 * To shutdown the service you should call stop() method
 */
public class ImageCrawler {

    public static final int NUMBER_OF_THREADS = 10;

    private ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    private String folder;

    public ImageCrawler(String folder) throws MalformedURLException {
        this.folder = folder;
    }

    /**
     * Call this method user start download images from specified URL.
     *
     * @param urlToPage
     * @throws IOException
     */
    public void downloadImages(String urlToPage) throws IOException {
        URL url = new URL(urlToPage);
        Page page = new Page(url);
        Collection<URL> imageLinks = page.getImageLinks();
        for (URL imageLink : imageLinks) {
            if (isImageURL(imageLink)) {
                executorService.submit(new ImageTask(imageLink, folder));
            }
        }
    }

    /**
     * Call this method before shutdown an application
     */
    public void stop() {
        executorService.shutdown();
    }

    //Help Mysienko Sergiy
    private boolean isImageURL(URL url) {
        Pattern pattern = Pattern.compile("([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)");
        Matcher matcher = pattern.matcher(url.toString());
        return matcher.matches();
    }


}
