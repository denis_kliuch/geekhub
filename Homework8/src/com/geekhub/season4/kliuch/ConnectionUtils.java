package com.geekhub.season4.kliuch;

import java.io.*;
import java.net.URL;

/**
 * Utils class that contains useful method user interact with URLConnection
 */
public class ConnectionUtils {

    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it user download big files it's dangerous.
     *
     * @param url
     * @return
     * @throws IOException
     */
    public static byte[] getData(URL url) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedInputStream in = new BufferedInputStream(url.openStream());
        byte data[] = new byte[1024];
        int count;
        while ((count = in.read(data, 0, 1024)) != -1) {
            byteArrayOutputStream.write(data, 0, count);
        }
        byteArrayOutputStream.flush();
        return byteArrayOutputStream.toByteArray();

    }
}


