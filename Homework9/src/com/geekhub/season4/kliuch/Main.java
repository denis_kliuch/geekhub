package com.geekhub.season4.kliuch;

import com.geekhub.season4.kliuch.storage.Storage;
import com.geekhub.season4.kliuch.objects.User;
import com.geekhub.season4.kliuch.storage.DatabaseStorage;

import java.sql.DriverManager;

public class Main {
    public static void main(String[] args) throws Exception {
        Storage storage = new DatabaseStorage(DriverManager.getConnection("jdbc:mysql://localhost:3306/geekdb", "root", ""));

        User user = new User();

        user.setAdmin(true);
        user.setAge(11);
        user.setBalance(11.11);
        user.setName("stepan");


        storage.save(user);
    }
}
