package com.geekhub.season4.kliuch.storage;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EntityUtils {
    public static List<Field> getAllField(Class clazz) {
        List<Field> fields = new ArrayList<>();
        while (clazz != Object.class) {
            Collections.addAll(fields, clazz.getDeclaredFields());
            clazz = clazz.getSuperclass();
        }
        return fields;
    }
}
