package com.geekhub.season4.kliuch.storage;

import com.geekhub.season4.kliuch.objects.Entity;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link Storage} that uses database as a storage for objects.
 * It uses simple object type names user define target table user save the object.
 * It uses reflection user access objects fields and retrieve data user map user database tables.
 * As an identifier it uses field id of {@link com.geekhub.season4.kliuch.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need user do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName();
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? new ArrayList<>() : result;
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        String sql = "DELETE FROM " + entity.getClass().getSimpleName() + " WHERE id = " + entity.getId();
        try (Statement statement = connection.createStatement()) {
            return statement.executeUpdate(sql) == 1;
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);
        String sql;
        if (entity.isNew()) {
            sql = createInsertQuery(entity.getClass().getSimpleName(), data);
        } else {
            sql = createUpdateQuery(entity.getClass().getSimpleName(), data);
        }
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            int iter = 1;
            for (Object value : data.values()) {
                preparedStatement.setObject(iter, value);
                iter++;
            }
            preparedStatement.executeUpdate();
            if (entity.isNew()) {
                ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
                generatedKeys.next();
                entity.setId(generatedKeys.getInt(1));
                generatedKeys.close();
            }
        }
    }

    public static String createInsertQuery(String tableName, Map<String, Object> data) throws SQLException {
        StringBuilder sqlInsert = new StringBuilder();

        StringBuilder columns = new StringBuilder();
        StringBuilder iterParam = new StringBuilder();
        sqlInsert.append("INSERT ")
                .append("INTO ")
                .append(tableName)
                .append(" (");
        for (String column : data.keySet()) {
            columns
                    .append(column)
                    .append(", ");
            iterParam.append("?, ");
        }
        columns.delete(columns.length() - 2, columns.length());
        iterParam.delete(iterParam.length() - 2, iterParam.length());
        sqlInsert
                .append(columns)
                .append(")")
                .append(" VALUES ")
                .append("(")
                .append(iterParam)
                .append(")")
                .append(";");
        return sqlInsert.toString();
    }

    public static String createUpdateQuery(String tableName, Map<String, Object> data) throws SQLException {
        StringBuilder sqlUpdate = new StringBuilder();

        sqlUpdate
                .append("UPDATE ")
                .append(tableName)
                .append(" SET ");
        for (String column : data.keySet()) {
            sqlUpdate
                    .append(column)
                    .append("=")
                    .append("?")
                    .append(",");
        }
        int currentQueryLength = sqlUpdate.length();
        sqlUpdate.delete(currentQueryLength - 1, currentQueryLength);
        sqlUpdate
                .append(" WHERE id")
                .append("=")
                .append(data.get("id"));
        return sqlUpdate.toString();
    }

    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        List<Field> fields = EntityUtils.getAllField(entity.getClass());
        Map<String, Object> result = new HashMap<>(fields.size());
        for (Field field : fields) {
            field.setAccessible(true);
            result.put(field.getName(), field.get(entity));
        }
        return result;
    }

    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {
        List<T> entities = new ArrayList<>();
        while (resultSet.next()) {
            entities.add(extractOne(clazz, resultSet));
        }
        return entities;
    }

    private <T extends Entity> T extractOne(Class<T> clazz, ResultSet resultSet) throws Exception {
        T entity = clazz.newInstance();
        List<Field> fields = EntityUtils.getAllField(clazz);
        for (Field field : fields) {
            field.setAccessible(true);
            field.set(entity, resultSet.getObject(field.getName()));
        }
        return entity;
    }
}
