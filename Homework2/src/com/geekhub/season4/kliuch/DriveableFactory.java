package com.geekhub.season4.kliuch;

import com.geekhub.season4.kliuch.forceacceptor.ForceAcceptor;
import com.geekhub.season4.kliuch.energyprovider.GasTank;
import com.geekhub.season4.kliuch.energyprovider.SolarBattery;
import com.geekhub.season4.kliuch.engine.GasEngine;
import com.geekhub.season4.kliuch.engine.SolarEngine;
import com.geekhub.season4.kliuch.forceacceptor.Propeller;
import com.geekhub.season4.kliuch.forceacceptor.Wheel;
import com.geekhub.season4.kliuch.vehicle.Boat;
import com.geekhub.season4.kliuch.vehicle.DieselCar;
import com.geekhub.season4.kliuch.vehicle.Vehicle;

public class DriveableFactory {

    public static Vehicle createGasCar() {
        ForceAcceptor[] wheels = {
                new Wheel(),
                new Wheel(),
                new Wheel(),
                new Wheel()
        };

        return new DieselCar(new GasEngine(), new GasTank(), wheels);
    }

    public static Vehicle createSolarCar() {
        ForceAcceptor[] wheels = {
                new Wheel(),
                new Wheel(),
                new Wheel(),
                new Wheel()
        };

        return new DieselCar(new SolarEngine(), new SolarBattery(), wheels);
    }

    public static Vehicle createBoat() {
        return new Boat(new GasEngine(), new GasTank(), new Propeller());
    }
}
