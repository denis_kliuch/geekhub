package com.geekhub.season4.kliuch.engine;

import com.geekhub.season4.kliuch.energyprovider.EnergyProvider;


public class GasEngine implements ForceProvider {
    private boolean isStarted = false;

    @Override
    public void startEngine(EnergyProvider energyProvider) {
        if (energyProvider.hasEnergy() && !isStarted) {
            System.out.println("Engine started");
            isStarted = true;
        }
    }

    @Override
    public void stopEngine() {
        if (isStarted) {
            System.out.println("Engine stopped");
            isStarted = false;
        }
    }
}

