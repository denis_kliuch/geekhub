package com.geekhub.season4.kliuch.engine;


import com.geekhub.season4.kliuch.energyprovider.EnergyProvider;

public interface ForceProvider {
    void startEngine(EnergyProvider energyProvider);
    void stopEngine();


}
