package com.geekhub.season4.kliuch.vehicle;

import com.geekhub.season4.kliuch.Direction;
import com.geekhub.season4.kliuch.energyprovider.EnergyProvider;
import com.geekhub.season4.kliuch.forceacceptor.ForceAcceptor;
import com.geekhub.season4.kliuch.engine.ForceProvider;
import com.geekhub.season4.kliuch.exceptions.TooMuchSpeedException;

public class DieselCar extends Vehicle {
    private ForceAcceptor[] wheels;
    private static final int MAX_SPEED = 200;
    private int speed;

    public DieselCar(ForceProvider forceProvider, EnergyProvider energyProvider, ForceAcceptor[] wheels) {
        super(forceProvider, energyProvider);
        this.wheels = wheels;
    }

    @Override
    public void accelerate(int increaseSpeed) {
        try {
            if (getEnergyProvider().hasEnergy() && (increaseSpeed + speed <= MAX_SPEED)) {
                for (ForceAcceptor wheel : wheels) {
                    wheel.run();
                }
                speed += increaseSpeed;
                System.out.println("Current speed = " + speed);
            } else {
                System.out.println("Can't star car, not enough fuel");
                throw new TooMuchSpeedException();
            }
        } catch (TooMuchSpeedException e) {
            e.printStackTrace();
        }
        setRunned(true);
    }

    @Override
    public void stop() {
        if (isRunned()) {
            for (ForceAcceptor wheel : wheels) {
                wheel.stop();
            }
            setRunned(false);
            System.out.println("Car stopped");
        } else {
            System.out.println("Car not started");
        }
    }


    @Override
    public void turn(Direction direction) {
        getSpeed();
        ForceAcceptor rightWheel = wheels[1];
        ForceAcceptor leftWheel = wheels[0];
        leftWheel.turnLeft(direction);
        rightWheel.turnRight(direction);
        getSpeed();
    }

    public int getSpeed() {
        return  getEnergyProvider().getEnergyCount() - 5;
    }
}
