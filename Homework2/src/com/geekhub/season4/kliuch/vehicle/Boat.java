package com.geekhub.season4.kliuch.vehicle;

import com.geekhub.season4.kliuch.Direction;
import com.geekhub.season4.kliuch.energyprovider.EnergyProvider;
import com.geekhub.season4.kliuch.forceacceptor.ForceAcceptor;
import com.geekhub.season4.kliuch.engine.ForceProvider;
import com.geekhub.season4.kliuch.exceptions.TooMuchSpeedException;

public class Boat extends Vehicle {
    private ForceAcceptor propeller;
    private static final int MAX_SPEED = 200;
    private int speed;

    public Boat(ForceProvider forceProvider, EnergyProvider energyProvider, ForceAcceptor propeller) {
        super(forceProvider, energyProvider);
        this.propeller = propeller;
    }

    @Override
    public void accelerate(int increaseSpeed) {

        try {
            if (increaseSpeed + speed <= MAX_SPEED) {
                    propeller.run();
                speed += increaseSpeed;
                System.out.println("Current speed = " + speed);
            } else {
                throw new TooMuchSpeedException();
            }
        } catch (TooMuchSpeedException e) {
            e.printStackTrace();
        }
        setRunned(true);
    }
    @Override
    public void stop() {
        if (isRunned()) {
            propeller.stop();
            setRunned(false);
        }
    }
//
//    @Override
//    public void turn(Direction direction) {
//        propeller.turn(direction);
//    }

    @Override
    public void turn(Direction direction) {

    }
}
