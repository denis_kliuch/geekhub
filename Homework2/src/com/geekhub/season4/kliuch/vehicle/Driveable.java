package com.geekhub.season4.kliuch.vehicle;

import com.geekhub.season4.kliuch.Direction;

public interface Driveable {
    void accelerate(int increaseSpeed);
    void stop();
    void turn(Direction direction);
}
