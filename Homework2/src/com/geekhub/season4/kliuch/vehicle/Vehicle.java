package com.geekhub.season4.kliuch.vehicle;

import com.geekhub.season4.kliuch.energyprovider.EnergyProvider;
import com.geekhub.season4.kliuch.engine.ForceProvider;

public abstract class Vehicle implements Driveable {
    private ForceProvider forceProvider;
    private EnergyProvider energyProvider;

    private boolean isRunned = false;

    protected Vehicle(ForceProvider forceProvider, EnergyProvider energyProvider) {
        this.forceProvider = forceProvider;
        this.energyProvider = energyProvider;
    }

    public ForceProvider getForceProvider() {
        return forceProvider;
    }

    public EnergyProvider getEnergyProvider() {
        return energyProvider;
    }

    public boolean isRunned() {
        return isRunned;
    }

    public void setRunned(boolean isRunned) {
        this.isRunned = isRunned;
    }
}
