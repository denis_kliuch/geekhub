package com.geekhub.season4.kliuch.energyprovider;


public interface EnergyProvider {

    void add(int count);

    void use(int count);

    int getEnergyCount();

    boolean hasEnergy();
}
