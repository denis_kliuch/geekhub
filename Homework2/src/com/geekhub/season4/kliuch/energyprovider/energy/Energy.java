package com.geekhub.season4.kliuch.energyprovider.energy;

public abstract class Energy {
    protected int energy;

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }
}
