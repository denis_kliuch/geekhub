package com.geekhub.season4.kliuch.energyprovider;

import com.geekhub.season4.kliuch.exceptions.GasTankOverFlowException;
import com.geekhub.season4.kliuch.energyprovider.energy.Energy;
import com.geekhub.season4.kliuch.energyprovider.energy.Solar;
import com.geekhub.season4.kliuch.exceptions.SolarTankOverFlowException;

public class SolarBattery implements EnergyProvider {

    private static final int MAX_SOLAR = 200;
    private Energy energy;

    public SolarBattery() {
        energy = new Solar();
    }

    @Override
    public void add(int count) {
        try {
            if (energy.getEnergy() + count < MAX_SOLAR) {
                energy.setEnergy(energy.getEnergy() + count);
                System.out.println("Fuel was added");
            } else {
                throw new SolarTankOverFlowException();
            }
        } catch (SolarTankOverFlowException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void use(int count) {
        try {
            if (energy.getEnergy() > count) {
                energy.setEnergy(energy.getEnergy() - count);

            } else {
                throw new GasTankOverFlowException();
            }
        } catch (GasTankOverFlowException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean hasEnergy() {
        if (energy.getEnergy() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public int getEnergyCount() {
        return energy.getEnergy();
    }
}
