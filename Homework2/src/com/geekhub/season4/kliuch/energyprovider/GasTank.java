package com.geekhub.season4.kliuch.energyprovider;

import com.geekhub.season4.kliuch.exceptions.GasTankOverFlowException;
import com.geekhub.season4.kliuch.energyprovider.energy.Energy;
import com.geekhub.season4.kliuch.energyprovider.energy.Gas;

public class GasTank implements EnergyProvider {

    private static final int MAX_FUEL = 100;
    private Energy energy;

    public GasTank() {
        energy = new Gas();
    }

    @Override
    public void add(int count) {
        try {
            if (energy.getEnergy() + count < MAX_FUEL) {
                energy.setEnergy(energy.getEnergy() + count);
                System.out.println("Fuel was added");
            } else {
                throw new GasTankOverFlowException();
            }
        } catch (GasTankOverFlowException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void use(int count) {
        if ( energy.getEnergy() > count)  {
            energy.setEnergy(energy.getEnergy() - count);

        } else {
            System.out.println("Not enough of fuel");
        }
    }

    @Override
    public boolean hasEnergy() {
        if (energy.getEnergy() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public int getEnergyCount() {
        return energy.getEnergy();
    }
}
