package com.geekhub.season4.kliuch;

import com.geekhub.season4.kliuch.vehicle.Vehicle;

import java.util.Scanner;

public class ApplicationMenu {

    private Vehicle vehicle;
    private Scanner in = new Scanner(System.in);

    public ApplicationMenu() {
        createVehicle();
    }

    private void createVehicle() {
        System.out.println("What type of vehicle do you want?");
        System.out.println("1. A gas car");
        System.out.println("2. A solar car");
        System.out.println("3. A boat");
        System.out.print("Enter a number: ");

        int menu = in.nextInt();

        switch (menu) {
            case 1:
                vehicle = DriveableFactory.createGasCar();
                break;
            case 2:
                vehicle = DriveableFactory.createSolarCar();
                break;
            case 3:
                vehicle = DriveableFactory.createBoat();
                break;
            default:
                System.out.println("Something wrong");
                return;
        }
        control();
    }

    private void control() {
        while (true) {
            System.out.print("1.Accelerating\n2.Turning left\n3.Turning right" +
                    "\n4.Stopping\n5.Add fuel\n6.Current fuel\nEnter a number: ");

            switch (in.nextInt()) {

                case 1:
                    System.out.print("Input speed: ");
                    vehicle.accelerate(in.nextInt());
                    break;

                case 2:
                    vehicle.turn(Direction.LEFT);
                    break;

                case 3:
                    vehicle.turn(Direction.RIGHT);
                    break;

                case 4:
                    vehicle.stop();
                    break;

                case 5:
                    System.out.print("Input fuel count: ");
                    vehicle.getEnergyProvider().add(in.nextInt());
                    break;

                case 6:
                    System.out.println("Fuel count = " + vehicle.getEnergyProvider().getEnergyCount());
                    break;

                default:
                    System.out.println("Error. Try again");
                    break;
            }
        }
    }
}
