package com.geekhub.season4.kliuch.forceacceptor;

import com.geekhub.season4.kliuch.Direction;

public class Wheel implements ForceAcceptor {

    @Override
    public void stop() {

    }

    @Override
    public void run() {

    }

    @Override
    public void turnRight(Direction direction) {
        if(direction == Direction.LEFT) {
            System.out.println("Turn left");
        }
    }

    @Override
    public void turnLeft(Direction direction) {
        if(direction == Direction.RIGHT) {

            System.out.println("Turn right");
        }
    }
}
