package com.geekhub.season4.kliuch.forceacceptor;

import com.geekhub.season4.kliuch.Direction;

public interface ForceAcceptor {
    void run();
    void turnRight(Direction direction);
    void turnLeft(Direction direction);
    void stop();
}
