package com.geekhub.season4.kliuch.exceptions;

public class NotEnoughOfEnergyException  extends Exception{
    public NotEnoughOfEnergyException() {
        super("NotEnoughOfFuelException");
    }
}
