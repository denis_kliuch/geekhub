package com.geekhub.season4.kliuch.part1;

import java.util.HashSet;
import java.util.Set;

public class Computation implements SetOperations {


    @Override
    public boolean equals(Set a, Set b) {
        if (a.containsAll(b)) {
            return true;
        }
        return false;
    }

    @Override
    public Set union(Set a, Set b) {
        Set c = new HashSet();
        c.addAll(a);
        c.addAll(b);
        return c;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set c = new HashSet();
        c.addAll(a);
        c.removeAll(b);
        return c;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set c = new HashSet();
        for(Object o : a ) {
            if (b.contains(o)) {
                c.add(o);
            }
        }
        return c;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        return union(subtract(a,b), subtract(a,b));
    }
}
