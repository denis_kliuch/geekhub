package com.geekhub.season4.kliuch.part1;

import java.util.Set;

public interface SetOperations {

    public boolean equals(Set a, Set b);

    public Set union(Set a, Set b);

    public Set subtract(Set a, Set b);

    public Set intersect(Set a, Set b);

    public Set symmetricSubtract(Set a, Set b);
}
