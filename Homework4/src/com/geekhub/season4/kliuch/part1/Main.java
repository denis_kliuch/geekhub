package com.geekhub.season4.kliuch.part1;


import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        SetOperations setOperations = new Computation();
        Set a = new HashSet();
        Set b = new HashSet();
        a.add(new String("Tato "));
        a.add(new String("muv "));
        a.add(new String("ramy "));
        b.add(new String("Mama "));
        b.add(new String("mula "));
        b.add(new String("ramy "));

        System.out.println("Equals: " + setOperations.equals(a, b));
        System.out.println("Union: " + setOperations.union(a, b));
        System.out.println("Subtract: " + setOperations.subtract(a,b));
        System.out.println("Intersect: " + setOperations.intersect(a,b));
        System.out.println("Symmetric Subtract: " + setOperations.symmetricSubtract(a,b));
    }
}
