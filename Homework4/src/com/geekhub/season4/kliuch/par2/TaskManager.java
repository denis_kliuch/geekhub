package com.geekhub.season4.kliuch.par2;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TaskManager {

    public void addTask(Date date, Task task);

    public void removeTask(Date date);

    public Collection<String> getCategories();

    public Map<String, List<Task>> getTaskByCategories();

    public List<Task> getTasksByCategory(String category);

    public List<Task> getTasksForToday();

}
