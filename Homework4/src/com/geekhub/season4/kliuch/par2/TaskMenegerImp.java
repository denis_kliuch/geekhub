package com.geekhub.season4.kliuch.par2;

import java.util.*;

public class TaskMenegerImp implements TaskManager {
    private Map<Date, Task> map = new HashMap<Date, Task>();

    @Override
    public void addTask(Date date, Task task) {
        map.put(date, task);
    }

    @Override
    public void removeTask(Date date) {
        map.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        Collection<String> result = new HashSet<String>();
        for (Task task : map.values()) {
            result.add(task.getCategory());
        }
        return result;
    }

    @Override
    public Map<String, List<Task>> getTaskByCategories() {
        //TODO: sort tasks by date, add if на провірку
        Map<String, List<Task>> result = new HashMap<String, List<Task>>();
        for (String category : getCategories()) {
            result.put(category, getTasksByCategory(category));
        }
        return result;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        //TODO: sort tasks by date
        List<Task> result = new ArrayList<Task>();
        for (Task task : map.values()) {
            if (task.getCategory().equals(category)) {
                result.add(task);
            }
        }
        return result;
    }

    @Override
    public List<Task> getTasksForToday() {
        //TODO: sort tasks by date
        List<Task> result = new ArrayList<Task>();
        Date today = new Date();
        for (Date taskDate : map.keySet()) {
            if (taskDate.equals(today)) {
                result.add(map.get(taskDate));
            }
        }
        return result;
    }
}
