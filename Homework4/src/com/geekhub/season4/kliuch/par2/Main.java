package com.geekhub.season4.kliuch.par2;

import java.util.Calendar;

public class Main {
    public static void main(String[] args) {
        Task task1 = new Task("movie", "01_12");
        Task task2 = new Task("movie", "01_10");
        Task task3 = new Task("book", "01_11");
        Task task4 = new Task("actor", "01_14");

        Calendar calendar = Calendar.getInstance();

        TaskMenegerImp manager = new TaskMenegerImp();

        calendar.set(2014, 12, 12);
        manager.addTask(calendar.getTime(), task1);
        calendar.set(2014, 12, 10);
        manager.addTask(calendar.getTime(), task2);
        calendar.set(2014, 12, 11);
        manager.addTask(calendar.getTime(), task3);
        calendar.set(2014, 12, 14);
        manager.addTask(calendar.getTime(), task4);

        System.out.println(manager.getCategories());
    }
}
