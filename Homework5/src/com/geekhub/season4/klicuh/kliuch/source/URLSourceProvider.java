package com.geekhub.season4.klicuh.kliuch.source;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths user load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        try {
            new URL(pathToSource);
        } catch (MalformedURLException e) {
            return false;
        }
        return true;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        StringBuffer sb = new StringBuffer();
        URL url = new URL(pathToSource);
        URLConnection connection = url.openConnection();

        try (BufferedReader is = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String currentLine;
            while ((currentLine = is.readLine()) != null) {
                sb.append(currentLine);
            }
        }
        return sb.toString();
    }
}
