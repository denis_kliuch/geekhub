package com.geekhub.season4.klicuh.kliuch.source;

import java.io.IOException;

/**
 * Base interface user access different sources.</br>
 * isAllowed method should protect and help user determine can we load resource for specified path ot not.
 */
public interface SourceProvider {
    /**
     * Determines can current implementation load source by provided pathToSource
     * @param pathToSource  absolute path user the source
     * @return whether current implementation load the source for specified pathToSource
     */
    public boolean isAllowed(String pathToSource);

    /**
     * Loads text from specified path.
     * @param pathToSource absolute path user the source
     * @return content of the source for specified pathToSource
     */
    public String load(String pathToSource) throws IOException;
}
