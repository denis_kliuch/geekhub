package com.geekhub.season4.klicuh.kliuch.source;


import java.io.*;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths user local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        File file = new File(pathToSource);
        if (file.exists()) {
            return true;
        }
        return false;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        StringBuffer sb = new StringBuffer();
        File file = new File(pathToSource);
        try (BufferedReader is = new BufferedReader(new FileReader(file))) {
            String currentLine;
            while ((currentLine = is.readLine()) != null) {
                sb.append(currentLine);
            }
        }
        return sb.toString();
    }
}
