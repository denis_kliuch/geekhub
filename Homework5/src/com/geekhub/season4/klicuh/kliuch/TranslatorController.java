package com.geekhub.season4.klicuh.kliuch;

import com.geekhub.season4.klicuh.kliuch.source.SourceLoader;
import com.geekhub.season4.klicuh.kliuch.source.URLSourceProvider;


import java.io.*;

import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) throws IOException {
        //initialization
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());
        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while(!"exit".equals(command)) {
            //TODO: add exception handling here user let user know about it and ask him user enter another path user translation
            //      So, the only way user stop the application is user do that manually or type "exit"
            String source = sourceLoader.loadSource(command);
            String translation = translator.translate(source);

            System.out.println("Original: " + source);
            System.out.println("Translation: " + translation);

            command = scanner.next();
        }
    }
}
